name := "licheat"

version := "0.1"

scalaVersion := "2.13.6"
// https://mvnrepository.com/artifact/org.slf4j/slf4j-api
libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.32"
// https://mvnrepository.com/artifact/org.slf4j/slf4j-simple
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.32"
libraryDependencies += "org.scalatestplus" %% "selenium-3-141" % "3.2.9.0"
libraryDependencies += "io.github.bonigarcia" % "webdrivermanager" % "5.0.3"
libraryDependencies += "org.scalatest" %% "scalatest-flatspec" % "3.2.9"
libraryDependencies += "org.scalatest" %% "scalatest-shouldmatchers" % "3.2.9"

libraryDependencies += "org.typelevel" %% "cats-core" % "2.1.0"
libraryDependencies += "org.typelevel" %% "cats-mtl" % "1.1.1"
libraryDependencies += "org.typelevel" %% "cats-effect" % "3.4-389-3862cf0"

libraryDependencies += "com.softwaremill.sttp.client3" %% "async-http-client-backend-cats" % "3.6.2"
libraryDependencies += "com.bot4s" %% "telegram-core" % "5.4.2"
libraryDependencies += "com.bot4s" %% "telegram-akka" % "5.4.2"

addCompilerPlugin("org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full)
scalacOptions ++= Seq(
  "-Xfatal-warnings",
  "-deprecation"
)