package app

import cats.effect.Async
import cats.mtl.Local

import java.time.Instant
import java.util.Date
import scala.language.implicitConversions

case class Cookie(
  name: String = "lila2",
  value: String,
  path: String = "/",
  expiryString: String,
  domain: String = ".lichess.org",
) {
  val expiry: Date = Date.from(Instant.parse(expiryString))
}

trait HasUser[E] {
  def cookie[F[_] : Async : Local[*[_], E]](env: E): F[Cookie]
}

final class HasUserOps[E](env: E)(implicit hasWebDriver: HasUser[E]) {
  def cookie[F[_] : Async : Local[*[_], E]]: F[Cookie] = hasWebDriver.cookie(env)
}

object HasUserSyntax {
  implicit def hasUserOps[E : HasUser](env: E): HasUserOps[E] = new HasUserOps[E](env)
}