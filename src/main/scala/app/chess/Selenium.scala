package app.chess

import app.{HasUser, HasWebDriver, Query}
import cats.effect.Async
import cats.mtl.Ask.ask
import cats.mtl.Local
import cats.syntax.flatMap._
import cats.syntax.functor._
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.{ExpectedCondition, ExpectedConditions, WebDriverWait}
import org.openqa.selenium.{By, WebDriver, WebElement}
import org.scalatestplus.selenium.{Page, WebBrowser}
import app.HasUserSyntax._
import app.HasWebDriverSyntax._

import scala.jdk.CollectionConverters._

class Selenium[
  E : HasUser : HasWebDriver,
  F[_] : Async : Local[*[_], E]
](password: String) {
  private def webDriverAction[A](f: WebDriver => A): F[A] = ask[F, E] >>= (_.webDriver.map(f))

  def acceptAlert: F[Unit] = webDriverAction { d =>
    val alert = d.switchTo().alert()
    alert.sendKeys(password)
    alert.accept()
  }

  def findElements(by: By): F[List[WebElement]] =
    webDriverAction(_.findElements(by).asScala.toList)

  def waitFor[T](
    condition: ExpectedCondition[T],
    timeOutInSecond: Long
  ): F[T] = webDriverAction { webDriver =>
    new WebDriverWait(
      webDriver,
      timeOutInSecond,
    ).until[T](condition)
  }

  def goToTournament(url: String): F[Unit] =
    webDriverAction { driver =>
      implicit val d: WebDriver = driver
      val returnUrl = url
      WebBrowser.go to new Page {
        val url: String = returnUrl
      }
    }

  def awaitPresence(by: By, timeoutInSeconds: Long): F[WebElement] =
    waitFor(ExpectedConditions.presenceOfElementLocated(by), timeoutInSeconds)

  def getClass(element: WebElement): String = element.getAttribute("class")

  def getSide(timeoutInSeconds: Long): F[String] =
    for {
      username <- awaitPresence(Query.Username, timeoutInSeconds)
      res <- awaitPresence(Query.side(username.getText), timeoutInSeconds)
    } yield if (res.getAttribute("class").contains("black")) "black" else "white"

  def click(element: WebElement): F[Unit] = webDriverAction { webDriver =>
    new Actions(webDriver).moveToElement(element).click().perform() }

  def addCookie(): F[Unit] = for {
    cookie <- ask[F, E] >>= (_.cookie)
    _ <- webDriverAction { webDriver =>
        implicit val d: WebDriver = webDriver
        import cookie._
        WebBrowser.addCookie(name, value, path, expiry, domain)
        WebBrowser.reloadPage()
      }
  } yield ()
}
