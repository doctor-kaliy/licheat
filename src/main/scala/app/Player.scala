package app

import app.chess.Selenium
import cats.effect.Async
import cats.mtl.Local

import scala.language.implicitConversions

trait Player[P] {
  def makeMove[
    E : HasWebDriver : HasUser,
    F[_] : Async : Local[*[_], E]
  ](
    player: P,
    side: String,
    selenium: Selenium[E, F]
  ): F[Unit]
}

class PlayerOps[P](p: P)(implicit player: Player[P]) {
  def makeMove[
    E : HasWebDriver : HasUser,
    F[_] : Async : Local[*[_], E]
  ](
    side: String,
    selenium: Selenium[E, F]
  ): F[Unit] = player.makeMove(p, side, selenium)
}

object PlayerSyntax {
  implicit def playerOps[P : Player](p: P): PlayerOps[P] = new PlayerOps[P](p)
}