package app

import app.chess.Selenium
import cats.effect.Async
import cats.mtl.Local
import cats.syntax.all._
import PlayerSyntax._

case class Game[
  P : Player,
  E : HasUser : HasWebDriver,
  F[_] : Async : Local[*[_], E]
](
  player: P,
  selenium: Selenium[E, F]
) {

  private def back(side: String): F[Unit] = (for {
    backButton <- selenium.awaitPresence(Query.BackButton, timeoutInSeconds = 5)
    _ <- selenium.click(backButton)
  } yield ()).redeemWith(_ => playNextMoves(side), _ => play())

  private def playNextMoves(side: String): F[Unit] = (for {
    _ <- selenium.awaitPresence(Query.Turn, timeoutInSeconds = 5)
    _ <- player.makeMove(side, selenium)
  } yield ()).redeemWith(_ => back(side), _ => playNextMoves(side))

  private def join: F[Unit] = for {
    join <- selenium.awaitPresence(Query.Join, timeoutInSeconds = 5)
    _ <- selenium.click(join)
    _ <- selenium.acceptAlert
  } yield ()

  private def playFirstMove(side: String): F[Unit] = for {
    _ <- selenium.awaitPresence(Query.Expiration, timeoutInSeconds = 20)
      .redeemWith(_ => back(side), _ => ().pure)
    _ <- player.makeMove(side, selenium)
    _ <- playNextMoves(side)
  } yield ()

  private def pause: F[Unit] =
    selenium
      .awaitPresence(Query.Pause, timeoutInSeconds = 5)
      .redeemWith(_ => ().pure, _ => play())

  def play(): F[Unit] =
    join.handleErrorWith(_ => ().pure) >>
      selenium.getSide(timeoutInSeconds = 5)
        .redeemWith(_ => pause, playFirstMove)
}
