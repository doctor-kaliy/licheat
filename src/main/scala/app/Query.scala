package app

import org.openqa.selenium.By

object Query {
  implicit val bySpec: String => By = By.xpath

  def by(query: String)(implicit by: String => By): By = by(query)

  def piece(side: String): By = Query by
    s"//piece[contains(@class,'$side') and contains(@style,'t') and not(contains(@class, 'ghost'))]"

  def promotionChoice(side: String): By = Query by
    s"//piece[contains(@class,'$side') and not(contains(@style,'t')) and not(contains(@class, 'ghost'))]"

  def side(name: String): By = Query by
    "//div[contains(@class, 'player') and contains(@class, 'color-icon')]" +
      s"/a[@href='/@/$name' and contains(@class, 'user-link')]/.."

  val Turn: By = Query by
    "//div[contains(@class, 'running') and contains(@class, 'rclock-bottom')]"

  val Join: By = Query by
    "//button[contains(@class, 'fbt text highlight')]"

  val Pause: By = Query by
    "//button[contains(@class, 'fbt text')]"

  val OpponentsMove: By = Query by
    "//div[not(contains(@class, 'running')) and contains(@class, 'rclock-top')]"

  val OpponentsTurn: By = Query by
    "//div[contains(@class, 'running') and contains(@class, 'rclock-top')]"

  val Expiration: By = Query by
    "//div[contains(@class, 'expiration-bottom')]"

  val BackButton: By = Query by
    "//a[contains(@class, 'text fbt strong glowing') and contains(@href, 'tournament')]"

  val MoveDestination: By = Query by
    "(//square[contains(@class,'move-dest')])"

  val Username: By = Query by
    "//a[contains(@id,'user_tag')]"
}
