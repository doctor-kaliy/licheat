package app

import cats.effect.Async
import cats.mtl.Local
import cats.syntax.all._
import org.openqa.selenium.WebDriver
import org.scalatestplus.selenium.Chrome

case class Env(
  cookie: Cookie,
  driver: Option[WebDriver] = None,
) {
  val webDriver: WebDriver = driver.getOrElse(Chrome.webDriver)
}

object Env {
  implicit object HasWebDriverEnv extends HasWebDriver[Env] {
    def webDriver[F[_] : Async : Local[*[_], Env]](env: Env): F[WebDriver] = Async[F].delay(env.webDriver)
  }

  implicit object HasUserEnv extends HasUser[Env] {
    def cookie[F[_] : Async : Local[*[_], Env]](env: Env): F[Cookie] = env.cookie.pure
  }
}