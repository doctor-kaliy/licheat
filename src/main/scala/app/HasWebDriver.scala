package app

import cats.effect.Async
import cats.mtl.Local
import org.openqa.selenium.WebDriver

import scala.language.implicitConversions

trait HasWebDriver[E] {
  def webDriver[F[_] : Async : Local[*[_], E]](env: E): F[WebDriver]
}

final class HasWebDriverOps[E](env: E)(implicit hasWebDriver: HasWebDriver[E]) {
  def webDriver[F[_] : Async : Local[*[_], E]]: F[WebDriver] = hasWebDriver.webDriver(env)
}

object HasWebDriverSyntax {
  implicit def hasWebDriverOps[E : HasWebDriver](env: E): HasWebDriverOps[E] = new HasWebDriverOps[E](env)
}