package app

import app.chess.Selenium
import cats.effect.Async
import cats.effect.kernel.Temporal
import cats.effect.std.Random
import cats.mtl.Local
import cats.syntax.all._
import org.openqa.selenium.WebElement
import scala.concurrent.duration.DurationInt

class RandomPlayer

case class RandomPlayerImpl[
  E : HasWebDriver : HasUser,
  F[_] : Async : Local[*[_], E]
](selenium: Selenium[E, F]) {
  private def getAvailableFigures(side: String): F[List[WebElement]] = for {
    random <- Random.scalaUtilRandom[F]
    time <- random.nextIntBounded(1000)
    _ <- Temporal[F].sleep(time.millis)
    elements <- selenium.findElements(Query.piece(side))
    promotionElements <- selenium.findElements(Query.promotionChoice(side))
    figures <- random.shuffleList(if (promotionElements.isEmpty) elements else promotionElements)
  } yield figures

  private def getAvailableMoves(figure: WebElement): F[List[WebElement]] = for {
    _ <- selenium.click(figure)
    random <- Random.scalaUtilRandom[F]
    elements <- selenium.findElements(Query.MoveDestination)
    moves <- random.shuffleList(elements)
  } yield moves

  private def anyClicked(figures: List[WebElement]): F[Boolean] = {
    figures.foldLeft[F[Boolean]](false.pure) { (acc, el) =>
      val clickEl = selenium.click(el) map { _ => true }
      acc.redeemWith(_ => clickEl, res => if (res) res.pure else clickEl)
    }
  }

  def makeMove(side: String): F[Unit] = for {
    figures <- getAvailableFigures(side)
    moves = figures.map(getAvailableMoves)
    _ <- moves.foldLeft[F[Boolean]](false.pure)((acc, el) =>
      acc.redeemWith(_ => el >>= anyClicked, res => if (res) res.pure else el >>= anyClicked))
  } yield ()
}

object RandomPlayer {
  implicit object RandomPlayerInst extends Player[RandomPlayer] {
    def makeMove[
      E : HasWebDriver : HasUser,
      F[_] : Async : Local[*[_], E]
    ](
      player: RandomPlayer,
      side: String,
      selenium: Selenium[E, F]
    ): F[Unit] = RandomPlayerImpl(selenium).makeMove(side)
  }
}