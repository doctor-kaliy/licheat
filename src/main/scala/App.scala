import cats.effect.{ExitCode, IO, IOApp}
import io.github.bonigarcia.wdm.WebDriverManager

object App extends IOApp {
  def run(args: List[String]): IO[ExitCode] = {
    args match {
      case List(token) =>
        IO { WebDriverManager.chromedriver().setup() } >>
        new LichessBot[IO](token = token)
          .startPolling()
          .as(ExitCode.Success)
      case _ => IO(println("Expected one argument - token")).as(ExitCode.Success)
    }
  }
}
