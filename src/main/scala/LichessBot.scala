import app.chess.Selenium
import app.{Cookie, Env, Game, RandomPlayer}
import cats.data.ReaderT
import cats.effect.implicits.monadCancelOps_
import cats.effect.{Async, Temporal}
import cats.syntax.all._
import com.bot4s.telegram.api.declarative.{Commands, RegexCommands}
import com.bot4s.telegram.cats.Polling
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver

class LichessBot[F[_] : Async : Temporal](token: String)
  extends Bot[F](token)
    with Polling[F]
    with Commands[F]
    with RegexCommands[F] {

  onCommand("start") { implicit msg =>
    reply("Hello!").void
  }

  def playTournament(
    link: String,
    password: String,
    cookie: Cookie
  ): F[Unit] = {
    val selenium: Selenium[Env, ReaderT[F, Env, *]] = new Selenium(password)
    Async[F].delay(new ChromeDriver().asInstanceOf[WebDriver]).bracket { driver =>
      import selenium._
      (goToTournament(link) >>
        addCookie() >>
        Game(new RandomPlayer, selenium).play())
        .run(Env(cookie, Some(driver)))
    } { driver => Async[F].delay(driver.close()) }
  }

  onCommand("play") { implicit msg =>
    withArgs {
      case Seq(link, password, name, value, path, expiry, domain) =>
        playTournament(link, password, Cookie(name, value, path, expiry, domain)) >>
          reply("Tournament ended").void

      case Seq(link, password, value, expiry) =>
        playTournament(link, password, Cookie(value = value, expiryString = expiry))

      case _ => reply("Invalid arguments").void
    }
  }

}