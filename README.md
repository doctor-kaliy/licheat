Telegram bot that plays chess on [lichess.org](https://lichess.org).
Selenium is used so bot can go to a website and play.
You just need to send your lichess cookie to the bot.

Usage `/play <lichess tournament url> <tournament password> <cookie value> <cookie expiry>`

To start bot use `sbt run <telegram bot token>`
